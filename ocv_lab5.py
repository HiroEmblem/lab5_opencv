import sys
import cv2
import numpy as np
import os

close= 0
hsv_mode = True
thresholdValue = 5

hTresholdL=30
sTresholdL=150
vTresholdL=50

hTresholdH=255
sTresholdH=255
vTresholdH=180

is_camera = len(sys.argv) == 1 

if is_camera:
  cap = cv2.VideoCapture(0)
else:  
  frame = cv2.imread(sys.argv[1])

while close == 0:
  key_val = cv2.waitKey(1) & 0xFF
  if key_val == 27:       #Escape
    break
  elif key_val == 32:     #Espace
    hsv_mode = not hsv_mode
    cv2.destroyAllWindows()
  if hsv_mode == False:  
    if key_val == ord('-'):
      hTresholdH-=thresholdValue
      sTresholdH-=thresholdValue
      vTresholdH-=thresholdValue  
      print("high treshold : << % 2d, % 2d, % 2d >>" %(hTresholdH, sTresholdH, vTresholdH))
      print("low treshold : << % 2d, % 2d, % 2d >>" %(hTresholdL, sTresholdL, vTresholdL))   
    elif key_val == ord('='):
      hTresholdH+=thresholdValue
      sTresholdH+=thresholdValue
      vTresholdH+=thresholdValue 
      print("high treshold : << % 2d, % 2d, % 2d >>" %(hTresholdH, sTresholdH, vTresholdH))
      print("low treshold : << % 2d, % 2d, % 2d >>" %(hTresholdL, sTresholdL, vTresholdL))   
    elif key_val == ord('9'):
      hTresholdL-=thresholdValue
      sTresholdL-=thresholdValue
      vTresholdL-=thresholdValue   
      print("high treshold : << % 2d, % 2d, % 2d >>" %(hTresholdH, sTresholdH, vTresholdH))
      print("low treshold : << % 2d, % 2d, % 2d >>" %(hTresholdL, sTresholdL, vTresholdL))   
    elif key_val == ord('0'):
      hTresholdL+=thresholdValue
      sTresholdL+=thresholdValue
      vTresholdL+=thresholdValue 
      print("high treshold : << % 2d, % 2d, % 2d >>" %(hTresholdH, sTresholdH, vTresholdH))
      print("low treshold : << % 2d, % 2d, % 2d >>" %(hTresholdL, sTresholdL, vTresholdL))       
  
  if is_camera:
    ret, frame = cap.read()

  if hsv_mode == True:
    frameHSV = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV) 
    h,s,v = cv2.split(frameHSV)
    cv2.imshow('Originale', frame)
    cv2.imshow('HSV', frameHSV) 
    cv2.imshow('H', h)
    cv2.imshow('S', s)
    cv2.imshow('V', v)
  else:
    mask = cv2.inRange(frame,np.array([hTresholdL,sTresholdL,vTresholdL]), np.array([hTresholdH,sTresholdH,vTresholdH]))
    frameFilter = cv2.bitwise_and(frame,frame, mask= mask)
    cv2.imshow('Originale', frame)
    cv2.imshow('Mask', mask)
    cv2.imshow('Filter', frameFilter)

if is_camera:
  cap.release()
cv2.destroyAllWindows()